import { check } from "express-validator";

class PostValidation {
    addNewPost = [
        check("title")
            .not()
            .isEmpty()
            .withMessage("Please pass title.")
            .bail()
            .custom((value, { req }) => {
                if(!req.body.title.match(/^[0-9a-zA-Z ]+$/))
                    throw new Error("Please pass valid title, allowed space and alphanumeric character.")
                return true;
            }),
        check("content")
            .not()
            .isEmpty()
            .withMessage("Please pass content.")
    ]

    modifyPost = [
        check("title")
            .not()
            .isEmpty()
            .withMessage("Please pass title.")
            .bail()
            .custom((value, { req }) => {
                if(!req.body.title.match(/^[0-9a-zA-Z ]+$/))
                    throw new Error("Please pass valid title, allowed space and alphanumeric character.")
                return true;
            }),
        check("content")
            .not()
            .isEmpty()
            .withMessage("Please pass content.")
    ]
}

export const PostValidator = new PostValidation();