import express from "express";
const router = express.Router();
import { PostValidator } from "../middlewares/validators/Posts.validation";
import { postController } from "../controllers/Posts.controller";

router.get("/", postController.fetchAllPosts);

router.post("/", PostValidator.addNewPost, postController.addNewPost);

router.patch("/:id", PostValidator.modifyPost, postController.modifyPost);

router.get("/:id", postController.fetchPost);

router.delete("/:id", postController.deletePost);

export const PostRoutes = router;