import app from "./app";

const port : String = process.env.PORT || "3000";
 // To run the service on the port
app.listen(port, () => {
    console.log("Aplication running on the port ", port);
})