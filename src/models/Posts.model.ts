import {Model, Table, Column, DataType} from "sequelize-typescript";

@Table({
    tableName : "Posts",
    paranoid : true
})

export default class Posts extends Model {
    @Column({
        type : DataType.INTEGER,
        primaryKey : true,
        autoIncrement : true,
        field : "id"
    })
    id?: Number;

    @Column({
        type : DataType.TEXT,
        field : "title",
        unique : true
    })
    title? : Number;

    @Column({
        type : DataType.TEXT,
        field : "content"
    })
    content? : String;

    @Column({
        type : DataType.DATE,
        field : "created_at"
    })
    createdAt?: any;

    @Column({
        type : DataType.DATE,
        field : "updated_at"
    })
    updatedAt?: any;

    @Column({
        type : DataType.DATE,
        field : "deleted_at"
    })
    deletedAt?: any;
}

