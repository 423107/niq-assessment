import request from "supertest";
import app from "../app";

describe("Server.ts tests", () => {
    test("test", () => {
      expect(1 + 1).toBe(2);
    });
  });

describe("Test app.ts", () => {
    test("Check Simple route", async () => {
      const res = await request(app).get("/");
      // console.log(res.body);
      expect(res.body).toEqual({ message: "Hellow amol..!" });
    });
  });