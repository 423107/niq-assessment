import request from "supertest";
import app from "../../app";

describe("Posts routes", () => {
  test("Get all posts", async () => {
    const res = await request(app).get("/posts");
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(true);
  });

  var title : String = "";
  var post_id : Number = 0;

  test("Add new post", async () => {
    title = (Math.random() + 1).toString(36).substring(7);
    let content = (Math.random() + 1).toString(36).substring(7);

    const res = await request(app).post("/posts")
                    .send({
                        title : title,
                        content : content
                    });
    post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(true);
    expect(res.body.ResponseMessage).toBe("Post has been created successfully.");
    expect(typeof res.body.Response == 'object').toBe(true);
  });



  test("Add new post with existing title", async () => {
    let content = (Math.random() + 1).toString(36).substring(7);

    const res = await request(app).post("/posts")
                    .send({
                        title : title,
                        content : content
                    });
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(false);
    expect(res.body.ResponseMessage).toBe("Post aready exists, look like duplicate item.");
    expect(res.body.Response == null).toBe(true);
  });


  test("Update post with wrong post id", async () => {
    let content = (Math.random() + 1).toString(36).substring(7);

    const res = await request(app).patch("/posts/test")
                    .send({
                        title : title,
                        content : content
                    });
    // post_id = res.body.Response.id;
    console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(false);
    expect(res.body.ResponseMessage).toBe("Valid post id is required to fetch the details.");
    expect(res.body.Response == null).toBe(true);
  });


  test("Update post with valid post id but record not exists", async () => {
    let content = (Math.random() + 1).toString(36).substring(7);

    const res = await request(app).patch("/posts/423107")
                    .send({
                        title : title,
                        content : content
                    });
    // post_id = res.body.Response.id;
    console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(false);
    expect(res.body.ResponseMessage).toBe("Post not exists.");
    expect(res.body.Response == null).toBe(true);
  });


  test("Update post with existing title", async () => {
    let content = (Math.random() + 1).toString(36).substring(7);

    const res = await request(app).patch("/posts/"+post_id)
                    .send({
                        title : title,
                        content : content
                    });
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(false);
    expect(res.body.ResponseMessage).toBe("Post aready exists, look like duplicate item.");
    expect(res.body.Response == null).toBe(true);
  });


  test("Update post with new title", async () => {
    let content = (Math.random() + 1).toString(36).substring(7);
    title = (Math.random() + 1).toString(36).substring(7);
    const res = await request(app).patch("/posts/"+post_id)
                    .send({
                        title : title,
                        content : content
                    });
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(true);
    expect(res.body.ResponseMessage).toBe("Post updated successfully.");
    expect(res.body.Response == null).toBe(true);
  });


  test("Fetch post with wrong post id", async () => {
    const res = await request(app).get("/posts/test");
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(false);
    expect(res.body.ResponseMessage).toBe("Valid post id is required to fetch the details.");
    expect(res.body.Response == null).toBe(true);
  });


  test("Fetch post with valid post id but record not exists", async () => {
    const res = await request(app).get("/posts/423107");
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(false);
    expect(res.body.ResponseMessage).toBe("Post not exists.");
    expect(res.body.Response == null).toBe(true);
  });

  test("Fetch post", async () => {
    const res = await request(app).get("/posts/"+post_id);
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(true);
    expect(res.body.ResponseMessage).toBe("Operation Successfull");
    expect(res.body.Response != null).toBe(true);
  });



  test("Delete post with wrong post id", async () => {
    const res = await request(app).delete("/posts/test");
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(false);
    expect(res.body.ResponseMessage).toBe("Valid post id is required to fetch the details.");
    expect(res.body.Response == null).toBe(true);
  });


  test("Delete post with valid post id but record not exists", async () => {
    const res = await request(app).delete("/posts/423107");
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(false);
    expect(res.body.ResponseMessage).toBe("Post not exists.");
    expect(res.body.Response == null).toBe(true);
  });

  test("Delete post", async () => {
    const res = await request(app).delete("/posts/"+post_id);
    // post_id = res.body.Response.id;
    // console.log(res.body)
    expect(res.status).toBe(200);
    expect((typeof res.body == 'object')).toBe(true);
    expect(res.body.Success).toBe(true);
    expect(res.body.ResponseMessage).toBe("Post deleted successfully");
    expect(res.body.Response == null).toBe(true);
  });



});