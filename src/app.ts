import express, { Application, Request, Response, NextFunction } from "express";
import Database from "./database/Database.connection";
new Database();  // Connect DB
import { PostRoutes } from "./routes/Posts.routes";
import dotenv from "dotenv";
dotenv.config();   // Import an environment config

const app: Application = express();
app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use((req : Request, res : Response, next : NextFunction) => {
    if(typeof req.body != 'object')
        return res.status(400).send({
            Success : false,
            ResponseMessage : "Please pass valid json body.",
            Response : null
        })
    next();
})
app.use("/posts", PostRoutes);

app.use("/", (req: Request, res: Response, next: NextFunction): void => {
  res.json({ message: "Hellow amol..!" });
});

export default app;