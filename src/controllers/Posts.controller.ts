import {Request, Response} from "express";
import { validationResult } from "express-validator";
import Posts from "../models/Posts.model";

class PostController {
    fetchAllPosts = async (req : Request, res : Response) => {
        try{
            let filter = {
                limit : 5,
                offset : 0
            }

            if(req.params.limit != undefined)
                filter.limit = Number(req.params.limit)

            if(req.params.offset != undefined)
                filter.offset = Number(req.params.offset)

            const posts = await Posts.findAll({
                offset : filter.offset,
                limit : filter.limit
            })

            if(posts.length == 0){
                return res.send({
                    Success : true,
                    ResponseMessage : "No Record Found.",
                    Response : null
                })
            }

            return res.send({
                Success : true,
                ResponseMessage : "Operation Successfull",
                Response : posts
            })

        }catch(e) {
            return res.send({
                Success : false,
                ResponseMessage : "We are facing technical issue, please try after sometime.",
                Response : null
            })
        }
    }

    addNewPost = async (req : Request, res : Response) => {
        try {
            let errors = validationResult(req);
            let errors_array = errors.array();
            if(!errors.isEmpty() && errors_array.length >= 1){
                return res.send({
                    Success : false,
                    ResponseMessage : errors_array[0].msg,
                    Response : null
                })
            }
            const postExists = await Posts.count({
                where : {
                    title : req.body.title
                }
            })

            if(postExists != 0) {
                return res.send({
                    Success : false,
                    ResponseMessage : "Post aready exists, look like duplicate item.",
                    Response : null
                })
            }

            req.body = {...req.body, ...{created_by : Number(req.body.user_id), updated_by : null}}
            const grocery = await Posts.create(req.body);
            return res.send({
                Success : true,
                ResponseMessage : "Post has been created successfully.",
                Response : grocery
            })

        }catch (e) {
            console.log(e)
            return res.send({
                Success : false,
                ResponseMessage : "We are facing technical issue, please try after sometime.",
                Response : null
            })
        }
    }

    fetchPost = async (req : Request, res : Response) => {
        try {
            if(req.params.id == undefined || isNaN(Number(req.params.id)))
                return res.send({
                    Success : false,
                    ResponseMessage : "Valid post id is required to fetch the details.",
                    Response : null
                })
           
            const post = await Posts.findOne({where : {id : Number(req.params.id)}})
            if(post == null) {
                return res.send({
                    Success : false,
                    ResponseMessage : "Post not exists.",
                    Response : null
                })
            }

            return res.send({
                Success : true,
                ResponseMessage : "Operation Successfull",
                Response : post
            })
        }catch (e) {
            return res.send({
                Success : false,
                ResponseMessage : "We are facing technical issue, please try after sometime.",
                Response : null
            })
        }
    }

    modifyPost = async (req : Request, res : Response) => {
        try {
            let errors = validationResult(req);
            let errors_array = errors.array();
            if(!errors.isEmpty() && errors_array.length >= 1){
                return res.send({
                    Success : false,
                    ResponseMessage : errors_array[0].msg,
                    Response : null
                })
            }
            // console.log(typeof Number(req.params.id));
            if(req.params.id == undefined || isNaN(Number(req.params.id)))
                return res.send({
                    Success : false,
                    ResponseMessage : "Valid post id is required to fetch the details.",
                    Response : null
                })

            const post = await Posts.findOne({where : {id : Number(req.params.id)}})
            if(post == null) {
                return res.send({
                    Success : false,
                    ResponseMessage : "Post not exists.",
                    Response : null
                })
            }

            const postExists = await Posts.count({
                where : {
                    title : req.body.title
                }
            })

            if(postExists != 0) {
                return res.send({
                    Success : false,
                    ResponseMessage : "Post aready exists, look like duplicate item.",
                    Response : null
                })
            }

            const updatePost = await Posts.update({
                title : req.body.title,
                content : req.body.content
            },
                {
                    where : {id : Number(req.params.id)}
                })
            
            return res.send({
                Success : true,
                ResponseMessage : "Post updated successfully.",
                Response : null
            })
        }catch (e) {
            console.log(e)
            return res.send({
                Success : false,
                ResponseMessage : "We are facing technical issue, please try after sometime.",
                Response : null
            })
        }
    }

    deletePost = async (req : Request, res : Response) => {
        try {
            if(req.params.id == undefined || isNaN(Number(req.params.id)))
                return res.send({
                    Success : false,
                    ResponseMessage : "Valid post id is required to fetch the details.",
                    Response : null
                })

            const post = await Posts.findOne({where : {id : Number(req.params.id)}})
            if(post == null) {
                return res.send({
                    Success : false,
                    ResponseMessage : "Post not exists.",
                    Response : null
                })
            }

            await Posts.destroy({
                where : {id : Number(req.params.id)}
            })

            return res.send({
                Success : true,
                ResponseMessage : "Post deleted successfully",
                Response : null
            })

        }catch (e) {
            console.log(e)
            return res.send({
                Success : false,
                ResponseMessage : "We are facing technical issue, please try after sometime.",
                Response : null
            })
        }
    }
}

export const postController = new PostController();