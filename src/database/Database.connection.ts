import { Sequelize } from "sequelize-typescript";
import Posts from "../models/Posts.model";
import { DataTypes } from "sequelize";

class Database {
    public sequelize : Sequelize | undefined;
    public DB : any = "";    
    constructor () {
        this.DB = (process.env.NODE_ENV == "development") ? process.env.DEV_DB : (process.env.NODE_ENV == "testing") ? process.env.TEST_DB: process.env.PROD_DB;
        this.connectDatabase();
    }

    
    private async connectDatabase() {
        
        this.sequelize = new Sequelize({
            dialect : "sqlite",
            storage : this.DB,
            models : [Posts]
        })
        const queryInterface = this.sequelize.getQueryInterface();
        await this.sequelize.authenticate()
                    .then(async () => {
                        // await queryInterface.dropTable({tableName : "Posts"})
                        // Create table if not exists
                        await queryInterface.createTable('Posts', {
                            id : {
                                type : DataTypes.INTEGER,
                                primaryKey : true,
                                autoIncrement : true
                            },
                            title: {
                                type : DataTypes.TEXT,
                                unique : true
                            },
                            content: DataTypes.TEXT,
                            createdAt : {
                                type : DataTypes.DATE,
                                field : "created_at"
                            },
                            updatedAt : {
                                type : DataTypes.DATE,
                                field : "updated_at"
                            },
                            deletedAt : {
                                type : DataTypes.DATE,
                                field : "deleted_at"
                            }
                        });

                        // Insert bulk data
                        /*await queryInterface.bulkInsert('Posts', [{
                            title : 'one',
                            content : "Test One",
                            created_at : new Date(),
                            updated_at : null,
                            deleted_at : null
                        }, {
                            title : 'two',
                            content : "Test two",
                            created_at : new Date(),
                            updated_at : null,
                            deleted_at : null
                        }]);*/
                        console.log("Database connection has been established successfully.");
                    })
                    .catch((err) => {
                        console.log("Unable to connect to the database:", err);
                    })
    }
}
export default Database;