FROM node:20
WORKDIR /usr/src/app
COPY package*.json ./
COPY jest.config.js ./
RUN npm install
RUN npm i ts-jest
COPY tsconfig.json ./
COPY start.sh ./
COPY .env ./
COPY src ./src