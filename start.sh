#! /usr/bin/bash
npm run test
if [ $? -eq 0 ]
then
    echo "Tests passed successfully!";
    npm start
else
    echo "Tests failed!"
fi