# coding-test-node-js


## Instructions

Please implement CRUD functionality for managing post.  

Implementation expectations:

1. Close to a production ready app as possible
1. Readme.md file for instructions on how to run
1. Containerised app ready to ship api to a Kubernetes/Docker cluster
1. Apply TDD
1. Respect RESTful
1. Favour clean code, Design Pattern, Functional approach
1. Must use Github and send a url to a public Github url of your solution
	1. Please use git,  make incremental code commits to allows a history of you iterations.

### Integration and Deployment

- In the root attached the postman collection.
- File name - niq-assessment.postman_collection.json

### Master and Worker

There are 2 kinds of nodes.

- Master nodes (Masters)
- Worker nodes (Nodes)

Followed the below steps to do the integration and deployment.

- Create a Docker Image for a NodeJS application
- Publish that Image to DockerHub
- Create a kops Kubernetes Cluster
- Install Kubectl on our local machine
- Deploy our application into Kubernetes cluster

### Create a Docker Image

Clone it first!

```sh
mkdir coding-test-node-js

cd coding-test-node-js

git init

git remote add origin https://gitlab.com/423107/niq-assessment.git

git pull origin main

sudo docker-compose build

sudo docker-compose up
```

So we now have a NodeJS application that is already Dockerized!

### Publish an image on Dockerhub
Notice here,

```js
<YOUR DOCKER ID> -> is the Dockerhub Username
coding-test-node-js_posts-service -> is the image name and
```

Then see the image on the list locally.

```sh
docker image ls

REPOSITORY                                TAG                   IMAGE ID       CREATED        SIZE
amolnt/coding-test-node-js-posts-service  latest               13a974972901   2 hours ago    1.22GB
```

Then login to Dockerhub

```sh
docker login --username <YOUR DOCKER ID> 
```

It will ask for your password. give that and you should be good to go.

Then create tag and push the image

```sh
sudo docker tag coding-test-node-js-posts-service <YOUR DOCKER ID>/coding-test-node-js_posts-service

sudo docker push <YOUR DOCKER ID>/coding-test-node-js-posts-service
```

You can go to Dockerhub and verify that your image is there. For me the URL looks something like this:

#### Setup Kubernetes (K8s) Cluster on AWS

1.Create Ubuntu EC2 instance
2.install AWSCLI
```sh 
curl https://s3.amazonaws.com/aws-cli/awscli-bundle.zip -o awscli-bundle.zip
apt install unzip python
unzip awscli-bundle.zip
#sudo apt-get install unzip - if you dont have unzip in your system
./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
```
    
3.Install kubectl
```sh
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```
4.Create an IAM user/role  with Route53, EC2, IAM and S3 full access
5.Attach IAM role to ubuntu server

#### Note: If you create IAM user with programmatic access then provide Access keys. 
```sh 
aws configure
```
6.Install kops on ubuntu instance:
```sh
curl -LO https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x kops-linux-amd64
sudo mv kops-linux-amd64 /usr/local/bin/kops
```
7.Create a Route53 private hosted zone (you can create Public hosted zone if you have a domain)
8.create an S3 bucket 
```sh
aws s3 mb s3://dev.k8s.in
```
9.Expose environment variable:
```sh 
export KOPS_STATE_STORE=s3://dev.k8s.in
```
10.Create sshkeys before creating cluster
```sh
ssh-keygen
```
11.Create kubernetes cluster definitions on S3 bucket 
```sh 
kops create cluster --cloud=aws --zones=ap-southeast-1b --name=dev.k8s.in --node-count=2 --dns private
```
12.Create kubernetes cluser
```sh 
kops update cluster dev.k8s.in --yes
```
13.Validate your cluster 
```sh 
kops validate cluster
```

14. To list nodes
```sh 
kubectl get nodes 
```

#### Deploying NodeJS application container on Kubernetes 
1.Deploying NodeJS Application Container

- Before create changes in the below file.
- Filename - deployment.yml    line No - 17
- amolnt/coding-test-node-js-posts-service:latest  TO {YOUR DOCKER ID}/coding-test-node-js_posts-service:latest


```sh 
kubectl create -f deployment.yml
```
    
You can see the deployments by running the following command:

```sh
kubectl get deployments
```
and this will give an output like this

```sh
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
coding-test-node-js-posts-service   0/2     2            0           7s
```
   
### See the pods

Deployment has created 2 pods. Because we gave the option **replicas:2** in our deployment configuration.

Let's see those pods!

```sh
kubectl get pods
```

It will give you an output like this

```sh
NAME                                          READY   STATUS    RESTARTS   AGE
coding-test-node-js-posts-service-6fdf4bf45-pglg8   1/1     Running   0          49m
coding-test-node-js-posts-service-6fdf4bf45-s9dsd   1/1     Running   0          49m
```

If you want to delete the deployment you can run the following command:

```sh
kubectl delete deploy coding-test-node-js-posts-service
```

1.To delete cluster
```sh
kops delete cluster dev.k8s.in --yes
```

### Show it to public

Expose the deployment to the world using the following command

```sh
kubectl expose deployment coding-test-node-js-posts-service --type="LoadBalancer"
```

Then run

```sh
kubectl apply -f service.yml
```

And that will create a load balancer for our application.

```sh
http://<IP ADDR>:3000
```

and you will be greeted with the following output.

```json
{
"message": "Hellow amol..!"
}
```